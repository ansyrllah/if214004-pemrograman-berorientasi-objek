[🏡 home](../../../)

# Pertemuan 1 - Pemrograman Berorientasi Objek

## Learning Outcome
- Dapat menjelaskan konsep umum [pemrograman berorientasi objek](https://gitlab.com/marchgis/march-ed/wiki/-/wikis/oop) (OOP)
- Dapat menjelaskan pemanfaatan OOP di teknologi digital di industri
- Dapat menjelaskan konsep Class
- Dapat menjelaskan konsep Object
- Dapat menjelaskan konsep Attribute pada Class
- Dapat menjelaskan konsep Method pada Class
